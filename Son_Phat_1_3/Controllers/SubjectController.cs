﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Son_Phat_1_3.Models.View;
using Son_Phat_1_3.Models.Input;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SubjectController : Controller
    {
        private readonly Services.BLL.Subject helper;

        public SubjectController(Services.BLL.Subject helper)
        {
            this.helper = helper;
        }

        [HttpPost("SomeRecords")]
        public View<List<Models.Cassandra.Subject>> SomeRecords(List<Filter> filters)
        {
            List<Models.Cassandra.Subject> data = null;

            try
            {
                data = helper.SomeRecord(filters);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Subject>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Subject>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Subject>>
                .True(data);
        }
        [HttpGet("AllRecords")]
        public View<List<Models.Cassandra.Subject>> AllRecords()
        {
            List<Models.Cassandra.Subject> data = null;

            try
            {
                data = helper.SomeRecord(null);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Subject>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Subject>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Subject>>
                .True(data);
        }
        [HttpPost("Insert")]
        public View<Models.Cassandra.Subject> Insert(Models.Cassandra.Subject input)
        {
            Models.Cassandra.Subject data = null;
            input.ID = Guid.NewGuid();

            try
            {
                while (helper.isValid(input.ID))
                    input.ID = Guid.NewGuid();

                data = helper.Insert(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Subject>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Subject>
                .True(data);
        }
        [HttpPut("Update")]
        public View<Models.Cassandra.Subject> Update(Models.Cassandra.Subject input)
        {
            Models.Cassandra.Subject data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Subject>
                        .False(input, "Không Tồn Tại!", null);

                data = helper.Update(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Subject>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Subject>
                .True(data);
        }
        [HttpDelete("Delete")]
        public View<Models.Cassandra.Subject> Delete(Models.Cassandra.Subject input)
        {
            Models.Cassandra.Subject data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Subject>
                        .False(input, "Không Tồn Tại!", null);

                data = helper.Delete(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Subject>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Subject>
                .True(data);
        }
    }
}
