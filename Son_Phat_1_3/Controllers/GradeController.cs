﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Son_Phat_1_3.Models.View;
using Son_Phat_1_3.Models.Input;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GradeController : Controller
    {
        private readonly Services.BLL.Grade helper;

        public GradeController(Services.BLL.Grade helper)
        {
            this.helper = helper;
        }

        [HttpPost("SomeRecords")]
        public View<List<Models.Cassandra.Grade>> SomeRecords(List<Filter> filters)
        {
            List<Models.Cassandra.Grade> data = null;

            try
            {
                data = helper.SomeRecord(filters);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Grade>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Grade>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Grade>>
                .True(data);
        }
        [HttpGet("AllRecords")]
        public View<List<Models.Cassandra.Grade>> AllRecords()
        {
            List<Models.Cassandra.Grade> data = null;

            try
            {
                data = helper.SomeRecord(null);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Grade>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Grade>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Grade>>
                .True(data);
        }
        [HttpPost("Insert")]
        public View<Models.Cassandra.Grade> Insert(Models.Cassandra.Grade input)
        {
            Models.Cassandra.Grade data = null;
            input.ID = Guid.NewGuid();

            try
            {
                while (helper.isValid(input.ID))
                    input.ID = Guid.NewGuid();

                data = helper.Insert(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Grade>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Grade>
                .True(data);
        }
        [HttpPut("Update")]
        public View<Models.Cassandra.Grade> Update(Models.Cassandra.Grade input)
        {
            Models.Cassandra.Grade data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Grade>
                        .False(input, "Không Tồn Tại!", null);

                data = helper.Update(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Grade>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Grade>
                .True(data);
        }
        [HttpDelete("Delete")]
        public View<Models.Cassandra.Grade> Delete(Models.Cassandra.Grade input)
        {
            Models.Cassandra.Grade data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Grade>
                        .False(input, "Không Tồn Tại!", null);

                data = helper.Delete(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Grade>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Grade>
                .True(data);
        }
    }
}
