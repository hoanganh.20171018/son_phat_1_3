﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Son_Phat_1_3.Models.View;
using Son_Phat_1_3.Models.Input;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClassController : Controller
    {
        private readonly Services.BLL.Class helper;
        private readonly Services.BLL.Grade helperGrade;
        private readonly Services.BLL.Subject helperSubject;

        public ClassController(Services.BLL.Class helper, Services.BLL.Grade helperGrade, Services.BLL.Subject helperSubject)
        {
            this.helper = helper;
            this.helperGrade = helperGrade;
            this.helperSubject = helperSubject;
        }

        [HttpPost("SomeRecords")]
        public View<List<Models.Cassandra.Class>> SomeRecords(List<Filter> filters)
        {
            List<Models.Cassandra.Class> data = null;

            try
            {
                data = helper.SomeRecord(filters);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Class>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Class>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Class>>
                .True(data);
        }
        [HttpGet("AllRecords")]
        public View<List<Models.Cassandra.Class>> AllRecords()
        {
            List<Models.Cassandra.Class> data = null;

            try
            {
                data = helper.SomeRecord(null);

                if (data.Count == 0)
                    return View<List<Models.Cassandra.Class>>
                        .False(data, "Không Tồn Tại Danh Sách", null);
            }
            catch (Exception exception)
            {
                return View<List<Models.Cassandra.Class>>
                    .False(data, "Lỗi", exception);
            }

            return View<List<Models.Cassandra.Class>>
                .True(data);
        }
        [HttpPost("Insert")]
        public View<Models.Cassandra.Class> Insert(Models.Cassandra.Class input)
        {
            Models.Cassandra.Class data = null;
            input.ID = Guid.NewGuid();

            try
            {
                while (helper.isValid(input.ID))
                    input.ID = Guid.NewGuid();

                if (!helperGrade.isValid(input.GradeID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại Khối", null);

                if (!helperSubject.isValid(input.SubjectID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại Môn", null);

                data = helper.Insert(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Class>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Class>
                .True(data);
        }
        [HttpPut("Update")]
        public View<Models.Cassandra.Class> Update(Models.Cassandra.Class input)
        {
            Models.Cassandra.Class data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại!", null);

                if (!helperGrade.isValid(input.GradeID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại Khối", null);

                if (!helperSubject.isValid(input.SubjectID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại Môn", null);

                data = helper.Update(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Class>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Class>
                .True(data);
        }
        [HttpDelete("Delete")]
        public View<Models.Cassandra.Class> Delete(Models.Cassandra.Class input)
        {
            Models.Cassandra.Class data = null;

            try
            {
                if (!helper.isValid(input.ID))
                    return View<Models.Cassandra.Class>
                        .False(input, "Không Tồn Tại!", null);

                data = helper.Delete(input);
            }
            catch (Exception exception)
            {
                return View<Models.Cassandra.Class>
                    .False(data, "Lỗi", exception);
            }

            return View<Models.Cassandra.Class>
                .True(data);
        }
    }
}
