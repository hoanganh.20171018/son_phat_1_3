﻿using Cassandra;
using Cassandra.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Services
{
    public class CassandraConnect
    {
        public CassandraConnect()
        {
            MappingConfiguration.Global.Define(
                new Map<Models.Cassandra.Grade>()
                .TableName("grade")
                .PartitionKey(grade => grade.ID)
            );
            MappingConfiguration.Global.Define(
                new Map<Models.Cassandra.Subject>()
                .TableName("subject")
                .PartitionKey(subject => subject.ID)
            );
            MappingConfiguration.Global.Define(
                new Map<Models.Cassandra.Class>()
                .TableName("class")
                .PartitionKey(_class => _class.ID)
            );
        }
        public string ConnectionString()
        {
            return new CassandraConnectionStringBuilder()
            {
                ContactPoints = new string[] { "127.0.0.1" },
                Port = 9042,
                DefaultKeyspace = "hoang_anh_lab"
            }.ConnectionString;
        }
        public ICluster Cluster()
        {
            /*return new Builder()
                .AddContactPoint(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9042))//127.0.0.1//10.10.10.177
                .WithDefaultKeyspace("hoang_anh_lab")
                .Build();*/
            return new Builder()
                .WithConnectionString(ConnectionString())
                .Build();
        }
    }
}
