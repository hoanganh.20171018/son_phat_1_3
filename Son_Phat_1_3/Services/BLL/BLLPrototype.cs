﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Services.BLL
{
    interface BLLPrototype<T>
    {
        List<T> AllRecords();
        List<T> SomeRecord(List<Models.Input.Filter> filters = null);
        T Insert(T input);
        T Update(T input);
        T Delete(T input);
    }
}
