﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Data;
using Cassandra.Mapping;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Services.BLL
{
    public class Subject : BLLPrototype<Models.Cassandra.Subject>
    {
        private readonly CassandraConnect helper;

        public Subject(CassandraConnect helper)
        {
            this.helper = helper;
        }

        public List<Models.Cassandra.Subject> AllRecords()
        {
            return SomeRecord(null);
        }
        public List<Models.Cassandra.Subject> SomeRecord(List<Models.Input.Filter> filters = null)
        {
            List<Models.Cassandra.Subject> result = new List<Models.Cassandra.Subject>();
            string cql = "select * from subject";
            string where = " where ";

            if (filters is not null)
            {
                foreach (Models.Input.Filter filter in filters)
                {
                    if (where != " where ")
                        where += " and ";

                    where += $"{filter.Field} = {filter.ToValue()}";
                }
            }

            if (where != " where ")
                cql += where + " allow filtering";

            using (var isession = helper.Cluster().Connect())
            {
                result = new Mapper(isession)
                    .Fetch<Models.Cassandra.Subject>(cql).ToList();
            }

            return result;
        }
        public Models.Cassandra.Subject Insert(Models.Cassandra.Subject input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Insert(input);
            }

            return input;
        }
        public Models.Cassandra.Subject Update(Models.Cassandra.Subject input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Update(input);
            }

            return input;
        }
        public Models.Cassandra.Subject Delete(Models.Cassandra.Subject input)
        {
            /*List<Models.Cassandra.Class> backup = new Class(helper).OwnedBy(input);
            int i = 0;

            try
            {
                foreach (Models.Cassandra.Class _class in backup)
                {
                    new Class(helper).Delete(_class);

                    i++;
                }
            }
            catch
            {
                goto Backup;
            }

            using (var isession = helper.Cluster().Connect())
            {
                try
                {
                    new Mapper(isession).Delete(input);

                    goto Return;
                }
                catch
                {
                    new Mapper(isession).Insert(input);

                    goto Backup;
                }
            }

        Backup:
            {
                for (int j = 0; j < i; j++)
                    new Class(helper).Insert(backup[j]);

                return new Models.Cassandra.Subject();
            }

        Return:
            {
                return input;
            }*/
            using (CqlConnection connection = new CqlConnection(helper.ConnectionString()))
            {
                CqlBatchTransaction transaction = new CqlBatchTransaction(connection);

                try
                {
                    using (var isession = helper.Cluster().Connect())
                    {
                        List<Guid> backups = new Mapper(isession).Fetch<Guid>($"select id from class where subjectid = {input.ID} allow filtering").ToList();

                        if (backups.Count != 0)
                            foreach (Guid backup in backups)
                                new Mapper(isession).Delete<Models.Cassandra.Class>("where id = ?", backup);

                        new Mapper(isession).Delete(input);//new Mapper(isession).Delete<Models.Cassandra.Class>("where id = ?");
                    }

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();

                    return new Models.Cassandra.Subject();
                }
            }

            return input;
        }
        public bool isValid(Guid input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                return new Mapper(isession)
                    .Fetch<List<Models.Cassandra.Subject>>("select * from subject where id = ?", input).ToList().Count() != 0;
            }
        }
    }
}
