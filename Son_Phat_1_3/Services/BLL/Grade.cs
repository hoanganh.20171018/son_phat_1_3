﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Mapping;
using Cassandra.Data;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Services.BLL
{
    public class Grade : BLLPrototype<Models.Cassandra.Grade>
    {
        private readonly CassandraConnect helper;

        public Grade(CassandraConnect helper)
        {
            this.helper = helper;
        }

        public List<Models.Cassandra.Grade> AllRecords()
        {
            return SomeRecord(null);
        }
        public List<Models.Cassandra.Grade> SomeRecord(List<Models.Input.Filter> filters = null)
        {
            List<Models.Cassandra.Grade> result = new List<Models.Cassandra.Grade>();
            string cql = "select * from grade";
            string where = " where ";

            if (filters is not null)
            {
                foreach (Models.Input.Filter filter in filters)
                {
                    if (where != " where ")
                        where += " and ";

                    where += $"{filter.Field} = {filter.ToValue()}";
                }
            }

            if (where != " where ")
                cql += where + " allow filtering";

            using (var isession = helper.Cluster().Connect())
            {
                result = new Mapper(isession)
                    .Fetch<Models.Cassandra.Grade>(cql).ToList();
            }

            return result;
        }
        public Models.Cassandra.Grade Insert(Models.Cassandra.Grade input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Insert(input);
            }

            return input;
        }
        public Models.Cassandra.Grade Update(Models.Cassandra.Grade input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Update(input);
            }

            return input;
        }
        public Models.Cassandra.Grade Delete(Models.Cassandra.Grade input)
        {
            /*List<Models.Cassandra.Class> backup = new Class(helper).OwnedBy(input);
            int i = 0;

            try
            {
                foreach (Models.Cassandra.Class _class in backup)
                {
                    new Class(helper).Delete(_class);

                    i++;
                }
            }
            catch
            {
                goto Backup;
            }

            using (var isession = helper.Cluster().Connect())
            {
                try
                {
                    new Mapper(isession).Delete(input);

                    goto Return;
                }
                catch
                {
                    new Mapper(isession).Insert(input);

                    goto Backup;
                }
            }

        Backup:
            {
                for (int j = 0; j < i; j++)
                    new Class(helper).Insert(backup[j]);

                return new Models.Cassandra.Grade();
            }

        Return:
            {
                return input;
            }*/
            using (CqlConnection connection = new CqlConnection(helper.ConnectionString()))
            {
                CqlBatchTransaction transaction = new CqlBatchTransaction(connection);

                try
                {
                    using (var isession = helper.Cluster().Connect())
                    {
                        List<Guid> backups = new Mapper(isession).Fetch<Guid>($"select id from class where gradeid = {input.ID} allow filtering").ToList();

                        if (backups.Count != 0)
                            foreach (Guid backup in backups)
                                new Mapper(isession).Delete<Models.Cassandra.Class>("where id = ?", backup);

                        new Mapper(isession).Delete(input);
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();

                    return new Models.Cassandra.Grade();
                }
            }

            return input;
        }
        public bool isValid(Guid input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                return new Mapper(isession)
                    .Fetch<List<Models.Cassandra.Grade>>("select * from grade where id = ?", input).ToList().Count() != 0;
            }
        }
    }
}
