﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Mapping;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Services.BLL
{
    public class Class : BLLPrototype<Models.Cassandra.Class>
    {
        private readonly CassandraConnect helper;

        public Class(CassandraConnect helper)
        {
            this.helper = helper;
        }

        public List<Models.Cassandra.Class> AllRecords()
        {
            return SomeRecord(null);
        }
        public List<Models.Cassandra.Class> SomeRecord(List<Models.Input.Filter> filters = null)
        {
            List<Models.Cassandra.Class> result = new List<Models.Cassandra.Class>();
            string cql = "select * from class";
            string where = " where ";

            if (filters is not null)
            {
                foreach (Models.Input.Filter filter in filters)
                {
                    if (where != " where ")
                        where += " and ";

                    where += $"{filter.Field} = {filter.ToValue()}";
                }
            }

            if (where != " where ")
                cql += where + " allow filtering";

            using (var isession = helper.Cluster().Connect())
            {
                result = new Mapper(isession)
                    .Fetch<Models.Cassandra.Class>(cql).ToList();
            }

            return result;
        }
        public Models.Cassandra.Class Insert(Models.Cassandra.Class input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Insert(input);
            }

            return input;
        }
        public Models.Cassandra.Class Update(Models.Cassandra.Class input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Update(input);
            }

            return input;
        }
        public Models.Cassandra.Class Delete(Models.Cassandra.Class input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                new Mapper(isession).Delete(input);
            }

            return input;
        }
        public bool isValid(Guid input)
        {
            using (var isession = helper.Cluster().Connect())
            {
                return new Mapper(isession)
                    .Fetch<List<Models.Cassandra.Grade>>("select * from class where id = ?", input).ToList().Count() != 0;
            }
        }
        public List<Models.Cassandra.Class> OwnedBy(object input)
        {
            try
            {
                if (input is Models.Cassandra.Grade grade)
                    return new Mapper(helper.Cluster().Connect())
                        .Fetch<Models.Cassandra.Class>("select * from class where gradeid = ?", grade.ID).ToList();
                else if (input is Models.Cassandra.Subject subject)
                    return new Mapper(helper.Cluster().Connect())
                        .Fetch<Models.Cassandra.Class>("select * from class where subjectid = ?", subject.ID).ToList();
            }
            catch
            {
                return new Mapper(helper.Cluster().Connect())
                    .Fetch<Models.Cassandra.Class>("select * from class").ToList();
            }
            return new List<Models.Cassandra.Class>();
        }
    }
}
