﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Models.Cassandra
{
    public class Grade
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
