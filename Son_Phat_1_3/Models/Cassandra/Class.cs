﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Models.Cassandra
{
    public class Class
    {
        public Guid ID { get; set; }
        public Guid GradeID { get; set; }
        public Guid SubjectID { get; set; }
        public string Name { get; set; }
        public int Tuition { get; set; }
    }
}
