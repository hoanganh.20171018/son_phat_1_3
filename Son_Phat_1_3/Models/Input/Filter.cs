﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Models.Input
{
    public class Filter
    {
        public string Field { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public object ToValue()
        {
            switch ($"{Type}".ToLower())
            {
                case "int": return int.TryParse(Value, out int intValue) ? intValue : -1;
                case "guid": return Guid.TryParse(Value, out Guid guidValue) ? guidValue : Guid.Empty;
                default: return $"'{Value}'";
            }
        }
    }
}
