﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Son_Phat_1_3.Models.View
{
    public class View<T> where T: class
    {
        public T Data { get; set; }
        public bool State { get; set; }
        public string Message { get; set; }
        public Exception Exception
        {
            #if RELEASE
                private get;
            #else
                get;
            #endif
                set;
        }
        public string ExceptionMessage => Exception?.Message;

        public View(T data, bool state, string message, Exception exception)
        {
            Data = data;
            State = state;
            Message = message;
            Exception = exception;
        }

        public static View<T> True(T data)
        {
            return new View<T>(data, true, "", null);
        }
        public static View<T> False(T data, string message, Exception exception)
        {
            return new View<T>(data, false, message, exception);
        }
    }
}
